void main() {
  App app = App();
  app.display();
  app.printAppNameInCaps();
}

class App {
  String appName = "EskomSePush";
  String sector = "Consumer Solution";
  String developers = "Herman Maritz and Dan Wells";
  String yearWon = "2015";

  void display(){
    print("App name is $appName");
    print("Sector is $sector");
    print("developer are $developers");
    print("Year won $yearWon");
  }

  void printAppNameInCaps(){
    print(appName.toUpperCase());
  }
}